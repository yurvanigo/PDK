package MainProgram;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.Toolkit;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Yurvan Aodi
 */
public class MainMenu extends javax.swing.JFrame {

    private int widthPixelGreen;
    private int widthPixelBlueThree;
    private int widthPixelBlueOne;
    private int widthPixelBlueTwo;
    private int widthPixelRedThree;
    private int widthPixelRedOne;
    private int widthPixelRedTwo;
    private int heightPixel;
    private Socket socket;
    private PrintWriter out;
    private SocketClient socketClient;
    private boolean isSecure = false;
    ArrayList<DataButton> objButton = new ArrayList<>();
    public String tempBtnBlue = null;

    /**
     * Creates new form JframeButton
     */
    public MainMenu() {
        initComponents();
        setPixelSize();
        setInitialButtonSize();
        this.setLocationRelativeTo(null);
        clearButtonGreen();
        clearButtonBlue();
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
    }

    private void dialogConnection() {
        jDialog1.setVisible(true);
        jDialog1.setLocationRelativeTo(this);
        jDialog1.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    }

    private void setPixelSize() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int widthGreen = (int) (screenSize.getWidth() / 5);
        int height = (int) (screenSize.getHeight() / 8);
        int blueThreeSize = (widthGreen * 2) / 3;
        int blueTwoSize = (widthGreen * 2);

        setHeightPixel(height);
        setWidthPixelGreen(widthGreen);
        setWidthPixelBlueThree(blueThreeSize);
        setWidthPixelBlueTwo(blueTwoSize);
        setWidthPixelBlueOne(widthGreen);
        setWidthPixelRedThree(blueThreeSize);
        setWidthPixelRedTwo(blueTwoSize);
        setWidthPixelRedOne(widthGreen);
    }

    private void setInitialButtonSize() {
        ButtonBlue1.setSize(new Dimension(getWidthPixelGreen(), getHeightPixel()));
        ButtonBlue2.setSize(new Dimension(getWidthPixelGreen(), getHeightPixel()));
        ButtonBlue3.setSize(new Dimension(getWidthPixelGreen(), getHeightPixel()));
        ButtonBlue4.setSize(new Dimension(getWidthPixelGreen(), getHeightPixel()));
        ButtonBlue5.setSize(new Dimension(getWidthPixelGreen(), getHeightPixel()));
        ButtonBlue6.setSize(new Dimension(getWidthPixelGreen(), getHeightPixel()));
        ButtonBlue7.setSize(new Dimension(getWidthPixelGreen(), getHeightPixel()));
        ButtonBlue8.setSize(new Dimension(getWidthPixelGreen(), getHeightPixel()));
        ButtonBlue9.setSize(new Dimension(getWidthPixelGreen(), getHeightPixel()));
        ButtonBlue10.setSize(new Dimension(getWidthPixelGreen(), getHeightPixel()));
        ButtonBlue11.setSize(new Dimension(getWidthPixelGreen(), getHeightPixel()));
        ButtonBlue12.setSize(new Dimension(getWidthPixelGreen(), getHeightPixel()));
        ButtonBlue13.setSize(new Dimension(getWidthPixelGreen(), getHeightPixel()));
        ButtonBlue14.setSize(new Dimension(getWidthPixelGreen(), getHeightPixel()));
        ButtonBlue15.setSize(new Dimension(getWidthPixelGreen(), getHeightPixel()));
    }

    public int getHeightPixel() {
        return heightPixel;
    }

    public void setHeightPixel(int heightPixel) {
        this.heightPixel = heightPixel;
    }

    public int getWidthPixelGreen() {
        return widthPixelGreen;
    }

    public void setWidthPixelGreen(int widthPixelGreen) {
        this.widthPixelGreen = widthPixelGreen;
    }

    public int getWidthPixelBlueThree() {
        return widthPixelBlueThree;
    }

    public void setWidthPixelBlueThree(int widthPixelBlueThree) {
        this.widthPixelBlueThree = widthPixelBlueThree;
    }

    public int getWidthPixelBlueOne() {
        return widthPixelBlueOne;
    }

    public void setWidthPixelBlueOne(int widthPixelBlueOne) {
        this.widthPixelBlueOne = widthPixelBlueOne;
    }

    public int getWidthPixelBlueTwo() {
        return widthPixelBlueTwo;
    }

    public void setWidthPixelBlueTwo(int widthPixelBlueTwo) {
        this.widthPixelBlueTwo = widthPixelBlueTwo;
    }

    public int getWidthPixelRedThree() {
        return widthPixelRedThree;
    }

    public void setWidthPixelRedThree(int widthPixelRedThree) {
        this.widthPixelRedThree = widthPixelRedThree;
    }

    public int getWidthPixelRedOne() {
        return widthPixelRedOne;
    }

    public void setWidthPixelRedOne(int widthPixelRedOne) {
        this.widthPixelRedOne = widthPixelRedOne;
    }

    public int getWidthPixelRedTwo() {
        return widthPixelRedTwo;
    }

    public void setWidthPixelRedTwo(int widthPixelRedTwo) {
        this.widthPixelRedTwo = widthPixelRedTwo;
    }

    public String getTempBtnBlue() {
        return tempBtnBlue;
    }

    public void setTempBtnBlue(String tempBtnBlue) {
        this.tempBtnBlue = tempBtnBlue;
    }

    private void clearTempBtnBlue() {
        this.tempBtnBlue = null;
    }

    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainMenu().setVisible(true);
            }
        });
    }

//    private void checkStatusServer() {
//        while (true) {
//            try {
//                System.out.println("1");
//                sendMessage("ping");
//                Thread.sleep(5000);
//            } catch (InterruptedException ex) {
//                Logger.getLogger(MainMenu.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//    }

    public void error(String error) {
        if (error == null || error.equals("")) {
            return;
        }
//        JFrame parent = new JFrame();
//        JOptionPane optionPane = new JOptionPane(error, JOptionPane.ERROR_MESSAGE);
//        JDialog dialog = optionPane.createDialog(parent, "Error");
//        dialog.setVisible(true);
//        dialog.setAlwaysOnTop(true);

        JOptionPane.showMessageDialog(MainMenu.this, error, "Error", JOptionPane.ERROR_MESSAGE);
    }

    public void error(String error, String heading) {
        if (error == null || error.equals("")) {
            return;
        }
//        JFrame parent = new JFrame();
//        JOptionPane optionPane = new JOptionPane(error, JOptionPane.ERROR_MESSAGE);
//        JDialog dialog = optionPane.createDialog(parent, heading);
//        dialog.setVisible(true);
//        dialog.setAlwaysOnTop(true);
        JOptionPane.showMessageDialog(MainMenu.this, error, heading, JOptionPane.ERROR_MESSAGE);
    }

    public void appendnoNewLine(String msg) {
        clearButtonGreen();
        objButton.clear();
//        messagesField.append(msg);
//        messagesField.setCaretPosition(messagesField.getText().length());
        String role = msg.trim().toUpperCase();
        switch (role) {
            case "SSW":
                objButton.add(new DataButton(true, 1, "GEN"));
                objButton.add(new DataButton(false, 2, "TRACK AIR"));
                objButton.add(new DataButton(true, 3, "TRACK SURF"));
                objButton.add(new DataButton(true, 4, "TRACK SUBSRF"));
                objButton.add(new DataButton(true, 5, "IDENT CHANGE"));
                objButton.add(new DataButton(true, 6, "HMS"));
                objButton.add(new DataButton(true, 7, "LIOD"));
                objButton.add(new DataButton(true, 10, "DATALINK"));
                objButton.add(new DataButton(true, 11, "TPO"));
                objButton.add(new DataButton(true, 14, "76mm GUN ENGAGE "));
                break;
            case "ASW":
                objButton.add(new DataButton(true, 1, "GEN"));
                objButton.add(new DataButton(false, 2, "TRACK AIR"));
                objButton.add(new DataButton(true, 3, "TRACK SURF"));
                objButton.add(new DataButton(true, 4, "TRACK SUBSRF"));
                objButton.add(new DataButton(true, 5, "IDENT CHANGE"));
                objButton.add(new DataButton(true, 6, "HMS"));
                objButton.add(new DataButton(true, 7, "LIOD"));
                objButton.add(new DataButton(true, 10, "DATALINK"));
                objButton.add(new DataButton(true, 11, "TPO"));
                objButton.add(new DataButton(true, 14, "76mm GUN ENGAGE "));
                break;
            case "SPC":
                objButton.add(new DataButton(true, 1, "GEN"));
                objButton.add(new DataButton(false, 2, "TRACK AIR"));
                objButton.add(new DataButton(true, 3, "TRACK SURF"));
                objButton.add(new DataButton(true, 4, "TRACK SUBSRF"));
                objButton.add(new DataButton(true, 5, "IDENT CHANGE"));
                objButton.add(new DataButton(false, 6, "HMS"));
                objButton.add(new DataButton(false, 7, "LIOD"));
                objButton.add(new DataButton(true, 10, "DATALINK"));
                objButton.add(new DataButton(false, 11, "TPO"));
                objButton.add(new DataButton(false, 14, "76mm GUN ENGAGE "));
                break;
            case "AAW":
                objButton.add(new DataButton(true, 1, "GEN"));
                objButton.add(new DataButton(true, 2, "TRACK AIR"));
                objButton.add(new DataButton(false, 3, "TRACK SURF"));
                objButton.add(new DataButton(false, 4, "TRACK SUBSRF"));
                objButton.add(new DataButton(true, 5, "IDENT CHANGE"));
                objButton.add(new DataButton(false, 6, "HMS"));
                objButton.add(new DataButton(true, 7, "LIOD"));
                objButton.add(new DataButton(true, 10, "DATALINK"));
                objButton.add(new DataButton(false, 11, "TPO"));
                objButton.add(new DataButton(true, 14, "76mm GUN ENGAGE "));
                break;
            case "AWW":
                objButton.add(new DataButton(true, 1, "GEN"));
                objButton.add(new DataButton(true, 2, "TRACK AIR"));
                objButton.add(new DataButton(true, 3, "TRACK SURF"));
                objButton.add(new DataButton(false, 4, "TRACK SUBSRF"));
                objButton.add(new DataButton(true, 5, "IDENT CHANGE"));
                objButton.add(new DataButton(false, 6, "HMS"));
                objButton.add(new DataButton(true, 7, "LIOD"));
                objButton.add(new DataButton(true, 10, "DATALINK"));
                objButton.add(new DataButton(false, 11, "TPO"));
                objButton.add(new DataButton(true, 14, "76mm GUN ENGAGE "));
                break;
            case "APC":
                objButton.add(new DataButton(true, 1, "GEN"));
                objButton.add(new DataButton(true, 2, "TRACK AIR"));
                objButton.add(new DataButton(false, 3, "TRACK SURF"));
                objButton.add(new DataButton(false, 4, "TRACK SUBSRF"));
                objButton.add(new DataButton(true, 5, "IDENT CHANGE"));
                objButton.add(new DataButton(false, 6, "HMS"));
                objButton.add(new DataButton(false, 7, "LIOD"));
                objButton.add(new DataButton(true, 10, "DATALINK"));
                objButton.add(new DataButton(false, 11, "TPO"));
                objButton.add(new DataButton(false, 14, "76mm GUN ENGAGE "));
                break;
            case "PWO":
                objButton.add(new DataButton(true, 1, "GEN"));
                objButton.add(new DataButton(true, 2, "TRACK AIR"));
                objButton.add(new DataButton(true, 3, "TRACK SURF"));
                objButton.add(new DataButton(true, 4, "TRACK SUBSRF"));
                objButton.add(new DataButton(true, 5, "IDENT CHANGE"));
                objButton.add(new DataButton(true, 6, "HMS"));
                objButton.add(new DataButton(true, 7, "LIOD"));
                objButton.add(new DataButton(true, 10, "DATALINK"));
                objButton.add(new DataButton(true, 11, "TPO"));
                objButton.add(new DataButton(true, 14, "76mm GUN ENGAGE "));
                break;
            case "PC":
                objButton.add(new DataButton(true, 1, "GEN"));
                objButton.add(new DataButton(true, 2, "TRACK AIR"));
                objButton.add(new DataButton(true, 3, "TRACK SURF"));
                objButton.add(new DataButton(true, 4, "TRACK SUBSRF"));
                objButton.add(new DataButton(true, 5, "IDENT CHANGE"));
                objButton.add(new DataButton(false, 6, "HMS"));
                objButton.add(new DataButton(false, 7, "LIOD"));
                objButton.add(new DataButton(false, 10, "DATALINK"));
                objButton.add(new DataButton(false, 11, "TPO"));
                objButton.add(new DataButton(false, 14, "76mm GUN ENGAGE "));
                break;
            case "MAINT":
                objButton.add(new DataButton(true, 1, "GEN"));
                objButton.add(new DataButton(true, 2, "TRACK AIR"));
                objButton.add(new DataButton(true, 3, "TRACK SURF"));
                objButton.add(new DataButton(true, 4, "TRACK SUBSRF"));
                objButton.add(new DataButton(true, 5, "IDENT CHANGE"));
                objButton.add(new DataButton(false, 6, "HMS"));
                objButton.add(new DataButton(false, 7, "LIOD"));
                objButton.add(new DataButton(false, 10, "DATALINK"));
                objButton.add(new DataButton(false, 11, "TPO"));
                objButton.add(new DataButton(true, 14, "76mm GUN ENGAGE "));
                break;
            case "GSS":
                objButton.add(new DataButton(true, 1, "GEN"));
                objButton.add(new DataButton(false, 2, "TRACK AIR"));
                objButton.add(new DataButton(false, 3, "TRACK SURF"));
                objButton.add(new DataButton(false, 4, "TRACK SUBSRF"));
                objButton.add(new DataButton(false, 5, "IDENT CHANGE"));
                objButton.add(new DataButton(false, 6, "HMS"));
                objButton.add(new DataButton(false, 7, "LIOD"));
                objButton.add(new DataButton(false, 10, "DATALINK"));
                objButton.add(new DataButton(false, 11, "TPO"));
                objButton.add(new DataButton(false, 14, "76mm GUN ENGAGE "));
                break;
            default:
                System.out.println("Input not valid");
                break;
        }
        setButtonGreen(objButton);
    }

    private void clearButtonGreen() {
        ButtonGreen1.setText(null);
        ButtonGreen2.setText(null);
        ButtonGreen3.setText(null);
        ButtonGreen4.setText(null);
        ButtonGreen5.setText(null);
        ButtonGreen6.setText(null);
        ButtonGreen7.setText(null);
        ButtonGreen8.setText(null);
        ButtonGreen9.setText(null);
        ButtonGreen10.setText(null);
        ButtonGreen11.setText(null);
        ButtonGreen12.setText(null);
        ButtonGreen13.setText(null);
        ButtonGreen14.setText(null);
        ButtonGreen15.setText(null);
    }

    private void clearButtonBlue() {
        ButtonBlue1.setText(null);
        ButtonBlue2.setText(null);
        ButtonBlue3.setText(null);
        ButtonBlue4.setText(null);
        ButtonBlue5.setText(null);
        ButtonBlue6.setText(null);
        ButtonBlue7.setText(null);
        ButtonBlue8.setText(null);
        ButtonBlue9.setText(null);
        ButtonBlue10.setText(null);
        ButtonBlue11.setText(null);
        ButtonBlue12.setText(null);
        ButtonBlue13.setText(null);
        ButtonBlue14.setText(null);
        ButtonBlue15.setText(null);
        ButtonBlue16.setText(null);
        ButtonBlue17.setText(null);
        ButtonBlue18.setText(null);
        ButtonBlue19.setText(null);
        ButtonBlue20.setText(null);
    }

    private void clearButtonRed() {
        ButtonRed1.setText(null);
        ButtonRed2.setText(null);
        ButtonRed3.setText(null);
        ButtonRed4.setText(null);
        ButtonRed5.setText(null);
    }

    private void setBtnGen() {
        clearButtonBlue();
        ButtonBlue2.setText("PLAT REQ");
        ButtonBlue4.setText("SUW ADVICE");
        ButtonBlue5.setText("AAW ADVICE");
        ButtonBlue6.setText("SENSOR RANGE");
        ButtonBlue7.setText("START CPA");
        ButtonBlue8.setText("JOIN");
        ButtonBlue9.setText("ASSOC");
        ButtonBlue10.setText("INCL AAW");
        ButtonBlue11.setText("WEAPON RANGE");
        ButtonBlue12.setText("STOP CPA");
        ButtonBlue13.setText("DIS JOIN");
        ButtonBlue14.setText("DISSOC");
        ButtonBlue15.setText("EXCL AAW");
        ButtonBlue16.setText("HOLD FIRE");
        ButtonBlue17.setText("THROW OFF");
        ButtonBlue18.setText("MAN OB");
        ButtonBlue19.setText("COLL AVOID");
        ButtonBlue20.setText("CAMERA");
    }

    private void setBtnTrackAir() {
        clearButtonBlue();
        ButtonBlue1.setText("DR AIR");
        ButtonBlue2.setText("VIS-B AIR");
        ButtonBlue3.setText("POINT GEN");
        ButtonBlue6.setText("RAM AIR");
        ButtonBlue7.setText("ESM-B AIR");
        ButtonBlue8.setText("POINT AIR");
        ButtonBlue13.setText("KEEP LOST");
        ButtonBlue14.setText("CHANGE SOURCE");
        ButtonBlue15.setText("WIPE SOURCE");
        ButtonBlue17.setText("REPOSIT");
        ButtonBlue18.setText("CORR");
        ButtonBlue19.setText("WIPE");
    }

    private void setBtnTrackSurf() {
        clearButtonBlue();
        ButtonBlue1.setText("DR SURF");
        ButtonBlue2.setText("VIS-B SURF");
        ButtonBlue3.setText("POINT GEN");
        ButtonBlue6.setText("RAM SURF");
        ButtonBlue7.setText("ESM-B SURF");
        ButtonBlue8.setText("POINT ESMF IX");
        ButtonBlue10.setText("CHANGE VIDEO SOURCE");
        ButtonBlue13.setText("KEEP LOST");
        ButtonBlue14.setText("CHANGE SOURCE");
        ButtonBlue15.setText("WIPE SORCE");
        ButtonBlue17.setText("REPOSIT");
        ButtonBlue18.setText("CORR");
        ButtonBlue19.setText("WIPE");
    }

    private void setBtnTrackSubSurf() {
        clearButtonBlue();
        ButtonBlue1.setText("DR SUB");
        ButtonBlue2.setText("DATUM");
        ButtonBlue3.setText("POINT GEN");
        ButtonBlue6.setText("RAM SUBSURF");
        ButtonBlue7.setText("ESM-B SURSURF");
        ButtonBlue8.setText("POINT ASW");
        ButtonBlue11.setText("ACO-B");
        ButtonBlue13.setText("KEEP LOST");
        ButtonBlue14.setText("CHANGE SOURCE");
        ButtonBlue15.setText("WIPE SOURCE");
        ButtonBlue17.setText("REPOSIT");
        ButtonBlue18.setText("CORR");
        ButtonBlue19.setText("WIPE");
    }

    private void setBtnIdentChange() {
        clearButtonBlue();
        System.out.println("No Action");
    }

    private void setBtnHms() {
        clearButtonBlue();
        System.out.println("No Action");
    }

    private void setBtnLiod() {
        clearButtonBlue();
        ButtonBlue1.setText("LIOD CNTR TO KMF");
        ButtonBlue4.setText("AUTO TRACK");
        ButtonBlue6.setText("POWER ON");
        ButtonBlue8.setText("DESIG LIOD");
        ButtonBlue9.setText("TV CAM ON");
        ButtonBlue11.setText("POWER OFF");
        ButtonBlue12.setText("ASSIGN TO 76 GUN");
        ButtonBlue13.setText("BREAK LIOD");
        ButtonBlue14.setText("IR CAM ON");
        ButtonBlue15.setText("LASER FIRE MAN");
        ButtonBlue19.setText("LASER ON");

    }

    private void setBtndataLink() {
        clearButtonBlue();
        System.out.println("No Action");
    }

    private void setBtnTpo() {
        clearButtonBlue();
        System.out.println("No Action");
    }

    private void setBtnGunEngage() {
        clearButtonBlue();
        System.out.println("No Action");
    }

    private void setButtonGreen(ArrayList<DataButton> objButtons) {
        for (int i = 0; i < objButton.size(); i++) {
            if (objButtons.get(i).isStatus()) {
                switch (objButtons.get(i).getPosition()) {
                    case 1:
                        ButtonGreen1.setText(objButtons.get(i).getName());
                        break;
                    case 2:
                        ButtonGreen2.setText(objButtons.get(i).getName());
                        break;
                    case 3:
                        ButtonGreen3.setText(objButtons.get(i).getName());
                        break;
                    case 4:
                        ButtonGreen4.setText(objButtons.get(i).getName());
                        break;
                    case 5:
                        ButtonGreen5.setText(objButtons.get(i).getName());
                        break;
                    case 6:
                        ButtonGreen6.setText(objButtons.get(i).getName());
                        break;
                    case 7:
                        ButtonGreen7.setText(objButtons.get(i).getName());
                        break;
                    case 8:
                        ButtonGreen7.setText(objButtons.get(i).getName());
                        break;
                    case 9:
                        ButtonGreen7.setText(objButtons.get(i).getName());
                        break;
                    case 10:
                        ButtonGreen10.setText(objButtons.get(i).getName());
                        break;
                    case 11:
                        ButtonGreen11.setText(objButtons.get(i).getName());
                        break;
                    case 12:
                        ButtonGreen12.setText(objButtons.get(i).getName());
                        break;
                    case 13:
                        ButtonGreen13.setText(objButtons.get(i).getName());
                        break;
                    case 14:
                        ButtonGreen14.setText(objButtons.get(i).getName());
                        break;
                    case 15:
                        ButtonGreen15.setText(objButtons.get(i).getName());
                        break;
                    default:
                        break;
                }
            }
        }
    }

    private void connect() {
        if (socket != null) {
            disconnect();
            return;
        }
        String ip = ipField.getText();
        String port = portField.getText();
        if (ip == null || ip.equals("")) {
            JOptionPane.showMessageDialog(MainMenu.this, "No IP Address. Please enter IP Address",
                    "Error connecting", JOptionPane.ERROR_MESSAGE);
//            statusServer.setText("No IP Address. Please enter IP Address");
            ipField.requestFocus();
            ipField.selectAll();
            return;
        }
        if (port == null || port.equals("")) {
            JOptionPane.showMessageDialog(MainMenu.this, "No Port number. Please enter Port number",
                    "Error connecting", JOptionPane.ERROR_MESSAGE);
//            statusServer.setText("No Port number. Please enter Port number");
            portField.requestFocus();
            portField.selectAll();
            return;
        }
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        if (!Util.checkHost(ip)) {
            JOptionPane.showMessageDialog(MainMenu.this, "Bad IP Address", "Error connecting", JOptionPane.ERROR_MESSAGE);
//            statusServer.setText("Bad IP Address , Error connecting");
            ipField.requestFocus();
            ipField.selectAll();
            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            return;
        }
        int portNo = 0;
        try {
            portNo = Integer.parseInt(port);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(MainMenu.this,
                    "Bad Port number. Please enter Port number",
                    "Error connecting", JOptionPane.ERROR_MESSAGE);
//            statusServer.setText( "Bad Port number. Please enter Port number Error connecting");
            portField.requestFocus();
            portField.selectAll();
            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            return;
        }

        try {
            if (isSecure == false) {
                System.out.println("Connectig in normal mode : " + ip + ":" + portNo);
                socket = new Socket(ip, portNo);
//                txtStatus.setForeground(Color.GREEN);
//                txtStatus.setText("Connected");
                jDialog1.dispose();
                setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }
//            connectButton.setText("Disconnect");
//            connectButton.setMnemonic('D');
//            connectButton.setToolTipText("Stop Connection");
        } catch (IOException e) {
            error(e.getMessage(), "Opening connection");
            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            return;
        }

        socketClient = SocketClient.handle(this, socket);

//        Runnable r = new Runnable() {
//            @Override
//            public void run() {
//                checkStatusServer();
//            }
//        };
//        new Thread(r).start();
    }

    public synchronized void disconnect() {
        try {
            socketClient.setDesonnected(true);
            socket.close();
        } catch (IOException e) {
            System.err.println("Error closing client : " + e);
        }
        socket = null;
        out = null;
        ipField.setEditable(true);
        portField.setEditable(true);
//        txtStatus.setForeground(Color.RED);
//        txtStatus.setText("Disconnected");
//        connectButton.setText("Connect");
//        connectButton.setMnemonic('C');
        connectButton.setToolTipText("Start Connection");
    }

    public void sendMessage(String s) {
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        try {
            if (out == null) {
                out = new PrintWriter(new BufferedWriter(
                        new OutputStreamWriter(socket.getOutputStream())), true);
            }
            out.print(s);
            out.flush();
            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        } catch (Exception e) {
            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            JOptionPane.showMessageDialog(MainMenu.this, e.getMessage(), "Server Disconnected", JOptionPane.ERROR_MESSAGE);
            dialogConnection();
            disconnect();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jDialog1 = new javax.swing.JDialog();
        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        num1 = new javax.swing.JButton();
        num2 = new javax.swing.JButton();
        num3 = new javax.swing.JButton();
        num4 = new javax.swing.JButton();
        num5 = new javax.swing.JButton();
        num6 = new javax.swing.JButton();
        num7 = new javax.swing.JButton();
        num8 = new javax.swing.JButton();
        num9 = new javax.swing.JButton();
        numC = new javax.swing.JButton();
        num0 = new javax.swing.JButton();
        numDot = new javax.swing.JButton();
        portField = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        ipField = new javax.swing.JTextField();
        connectButton = new javax.swing.JButton();
        closeApplication = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        ButtonGreen1 = new javax.swing.JButton();
        ButtonGreen2 = new javax.swing.JButton();
        ButtonGreen3 = new javax.swing.JButton();
        ButtonGreen4 = new javax.swing.JButton();
        ButtonGreen5 = new javax.swing.JButton();
        ButtonGreen6 = new javax.swing.JButton();
        ButtonGreen7 = new javax.swing.JButton();
        ButtonGreen8 = new javax.swing.JButton();
        ButtonGreen9 = new javax.swing.JButton();
        ButtonGreen10 = new javax.swing.JButton();
        ButtonGreen11 = new javax.swing.JButton();
        ButtonGreen12 = new javax.swing.JButton();
        ButtonGreen13 = new javax.swing.JButton();
        ButtonGreen14 = new javax.swing.JButton();
        ButtonGreen15 = new javax.swing.JButton();
        ButtonBlue1 = new javax.swing.JButton();
        ButtonBlue2 = new javax.swing.JButton();
        ButtonBlue3 = new javax.swing.JButton();
        ButtonBlue4 = new javax.swing.JButton();
        ButtonBlue5 = new javax.swing.JButton();
        ButtonBlue6 = new javax.swing.JButton();
        ButtonBlue7 = new javax.swing.JButton();
        ButtonBlue8 = new javax.swing.JButton();
        ButtonBlue9 = new javax.swing.JButton();
        ButtonBlue10 = new javax.swing.JButton();
        ButtonBlue11 = new javax.swing.JButton();
        ButtonBlue12 = new javax.swing.JButton();
        ButtonBlue13 = new javax.swing.JButton();
        ButtonBlue14 = new javax.swing.JButton();
        ButtonBlue15 = new javax.swing.JButton();
        ButtonBlue16 = new javax.swing.JButton();
        ButtonBlue17 = new javax.swing.JButton();
        ButtonBlue18 = new javax.swing.JButton();
        ButtonBlue19 = new javax.swing.JButton();
        ButtonBlue20 = new javax.swing.JButton();
        ButtonRed1 = new javax.swing.JButton();
        ButtonRed2 = new javax.swing.JButton();
        ButtonRed3 = new javax.swing.JButton();
        ButtonRed4 = new javax.swing.JButton();
        ButtonRed5 = new javax.swing.JButton();

        jDialog1.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        jDialog1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jDialog1.setFocusable(false);
        jDialog1.setMinimumSize(new java.awt.Dimension(360, 500));
        jDialog1.setUndecorated(true);
        jDialog1.setResizable(false);
        jDialog1.setSize(new java.awt.Dimension(360, 500));

        jPanel2.setFocusTraversalPolicyProvider(true);
        jPanel2.setMaximumSize(new java.awt.Dimension(313, 500));
        jPanel2.setMinimumSize(new java.awt.Dimension(313, 500));
        jPanel2.setPreferredSize(new java.awt.Dimension(313, 500));
        jPanel2.setLayout(new java.awt.GridBagLayout());

        jLabel4.setText("Connect to Socket");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(29, 30, 0, 0);
        jPanel2.add(jLabel4, gridBagConstraints);

        num1.setText("1");
        num1.setFocusable(false);
        num1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                num1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.ipadx = 21;
        gridBagConstraints.ipady = 37;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 114, 0, 0);
        jPanel2.add(num1, gridBagConstraints);

        num2.setText("2");
        num2.setFocusable(false);
        num2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                num2ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.ipadx = 21;
        gridBagConstraints.ipady = 37;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 6, 0, 0);
        jPanel2.add(num2, gridBagConstraints);

        num3.setText("3");
        num3.setFocusable(false);
        num3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                num3ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 9;
        gridBagConstraints.ipadx = 21;
        gridBagConstraints.ipady = 37;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 6, 0, 112);
        jPanel2.add(num3, gridBagConstraints);

        num4.setText("4");
        num4.setFocusable(false);
        num4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                num4ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.ipadx = 21;
        gridBagConstraints.ipady = 37;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 114, 0, 0);
        jPanel2.add(num4, gridBagConstraints);

        num5.setText("5");
        num5.setFocusable(false);
        num5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                num5ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.ipadx = 21;
        gridBagConstraints.ipady = 37;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 6, 0, 0);
        jPanel2.add(num5, gridBagConstraints);

        num6.setText("6");
        num6.setFocusable(false);
        num6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                num6ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = 9;
        gridBagConstraints.ipadx = 21;
        gridBagConstraints.ipady = 37;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 6, 0, 112);
        jPanel2.add(num6, gridBagConstraints);

        num7.setText("7");
        num7.setFocusable(false);
        num7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                num7ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.ipadx = 21;
        gridBagConstraints.ipady = 37;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 114, 0, 0);
        jPanel2.add(num7, gridBagConstraints);

        num8.setText("8");
        num8.setFocusable(false);
        num8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                num8ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.ipadx = 21;
        gridBagConstraints.ipady = 37;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 6, 0, 0);
        jPanel2.add(num8, gridBagConstraints);

        num9.setText("9");
        num9.setFocusable(false);
        num9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                num9ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 9;
        gridBagConstraints.ipadx = 21;
        gridBagConstraints.ipady = 37;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 6, 0, 112);
        jPanel2.add(num9, gridBagConstraints);

        numC.setText("C");
        numC.setFocusable(false);
        numC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                numCActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.gridwidth = 9;
        gridBagConstraints.ipadx = 21;
        gridBagConstraints.ipady = 37;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 6, 0, 112);
        jPanel2.add(numC, gridBagConstraints);

        num0.setText("0");
        num0.setFocusable(false);
        num0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                num0ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.ipadx = 21;
        gridBagConstraints.ipady = 37;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 6, 0, 0);
        jPanel2.add(num0, gridBagConstraints);

        numDot.setText(".");
        numDot.setFocusable(false);
        numDot.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                numDotActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.ipadx = 23;
        gridBagConstraints.ipady = 37;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 114, 0, 0);
        jPanel2.add(numDot, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 8;
        gridBagConstraints.ipadx = 182;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 114, 0, 0);
        jPanel2.add(portField, gridBagConstraints);

        jLabel6.setText("IP Address");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(11, 114, 0, 0);
        jPanel2.add(jLabel6, gridBagConstraints);

        jLabel7.setText("Port");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 114, 0, 0);
        jPanel2.add(jLabel7, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 8;
        gridBagConstraints.ipadx = 182;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 114, 0, 0);
        jPanel2.add(ipField, gridBagConstraints);

        connectButton.setText("Connect");
        connectButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                connectButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 8;
        gridBagConstraints.ipadx = 115;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(11, 114, 0, 0);
        jPanel2.add(connectButton, gridBagConstraints);

        closeApplication.setText("Exit");
        closeApplication.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeApplicationActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.gridwidth = 13;
        gridBagConstraints.ipadx = 141;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 114, 34, 112);
        jPanel2.add(closeApplication, gridBagConstraints);

        jDialog1.getContentPane().add(jPanel2, java.awt.BorderLayout.CENTER);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setNextFocusableComponent(jPanel2);
        jPanel1.setRequestFocusEnabled(false);
        jPanel1.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                jPanel1AncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        jPanel1.setLayout(new java.awt.GridLayout(8, 5));

        ButtonGreen1.setBackground(new java.awt.Color(23, 156, 18));
        ButtonGreen1.setForeground(new java.awt.Color(255, 255, 255));
        ButtonGreen1.setText("GEN");
        ButtonGreen1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonGreen1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonGreen1ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonGreen1);

        ButtonGreen2.setBackground(new java.awt.Color(23, 156, 18));
        ButtonGreen2.setForeground(new java.awt.Color(255, 255, 255));
        ButtonGreen2.setText("TRACK AIR");
        ButtonGreen2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonGreen2.setMaximumSize(null);
        ButtonGreen2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonGreen2ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonGreen2);

        ButtonGreen3.setBackground(new java.awt.Color(23, 156, 18));
        ButtonGreen3.setForeground(new java.awt.Color(255, 255, 255));
        ButtonGreen3.setText("TRACK SURF");
        ButtonGreen3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonGreen3.setMaximumSize(null);
        ButtonGreen3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonGreen3ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonGreen3);

        ButtonGreen4.setBackground(new java.awt.Color(23, 156, 18));
        ButtonGreen4.setForeground(new java.awt.Color(255, 255, 255));
        ButtonGreen4.setText("TRACK SUBSRF");
        ButtonGreen4.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonGreen4.setMaximumSize(null);
        ButtonGreen4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonGreen4ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonGreen4);

        ButtonGreen5.setBackground(new java.awt.Color(23, 156, 18));
        ButtonGreen5.setForeground(new java.awt.Color(255, 255, 255));
        ButtonGreen5.setText("IDENT CHANGE");
        ButtonGreen5.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonGreen5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonGreen5ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonGreen5);

        ButtonGreen6.setBackground(new java.awt.Color(23, 156, 18));
        ButtonGreen6.setForeground(new java.awt.Color(255, 255, 255));
        ButtonGreen6.setText("HMS");
        ButtonGreen6.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonGreen6.setMaximumSize(null);
        ButtonGreen6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonGreen6ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonGreen6);

        ButtonGreen7.setBackground(new java.awt.Color(23, 156, 18));
        ButtonGreen7.setForeground(new java.awt.Color(255, 255, 255));
        ButtonGreen7.setText("L100");
        ButtonGreen7.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonGreen7.setMaximumSize(null);
        ButtonGreen7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonGreen7ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonGreen7);

        ButtonGreen8.setBackground(new java.awt.Color(23, 156, 18));
        ButtonGreen8.setForeground(new java.awt.Color(255, 255, 255));
        ButtonGreen8.setText("TRACK SURF");
        ButtonGreen8.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonGreen8.setMaximumSize(null);
        ButtonGreen8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonGreen8ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonGreen8);

        ButtonGreen9.setBackground(new java.awt.Color(23, 156, 18));
        ButtonGreen9.setForeground(new java.awt.Color(255, 255, 255));
        ButtonGreen9.setText("DATA LINK");
        ButtonGreen9.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonGreen9.setMaximumSize(null);
        ButtonGreen9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonGreen9ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonGreen9);

        ButtonGreen10.setBackground(new java.awt.Color(23, 156, 18));
        ButtonGreen10.setForeground(new java.awt.Color(255, 255, 255));
        ButtonGreen10.setText("DATA LINK");
        ButtonGreen10.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonGreen10.setMaximumSize(null);
        ButtonGreen10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonGreen10ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonGreen10);

        ButtonGreen11.setBackground(new java.awt.Color(23, 156, 18));
        ButtonGreen11.setForeground(new java.awt.Color(255, 255, 255));
        ButtonGreen11.setText("TPO");
        ButtonGreen11.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonGreen11.setMaximumSize(null);
        ButtonGreen11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonGreen11ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonGreen11);

        ButtonGreen12.setBackground(new java.awt.Color(23, 156, 18));
        ButtonGreen12.setForeground(new java.awt.Color(255, 255, 255));
        ButtonGreen12.setText("TRACK AIR");
        ButtonGreen12.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonGreen12.setMaximumSize(null);
        ButtonGreen12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonGreen12ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonGreen12);

        ButtonGreen13.setBackground(new java.awt.Color(23, 156, 18));
        ButtonGreen13.setForeground(new java.awt.Color(255, 255, 255));
        ButtonGreen13.setText("TRACK SURF");
        ButtonGreen13.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonGreen13.setMaximumSize(null);
        ButtonGreen13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonGreen13ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonGreen13);

        ButtonGreen14.setBackground(new java.awt.Color(23, 156, 18));
        ButtonGreen14.setForeground(new java.awt.Color(255, 255, 255));
        ButtonGreen14.setText("TRACK SUBSRF");
        ButtonGreen14.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonGreen14.setMaximumSize(null);
        ButtonGreen14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonGreen14ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonGreen14);

        ButtonGreen15.setBackground(new java.awt.Color(23, 156, 18));
        ButtonGreen15.setForeground(new java.awt.Color(255, 255, 255));
        ButtonGreen15.setText("IDENT CHANGE");
        ButtonGreen15.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonGreen15.setMaximumSize(null);
        ButtonGreen15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonGreen15ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonGreen15);

        ButtonBlue1.setBackground(new java.awt.Color(0, 131, 255));
        ButtonBlue1.setForeground(new java.awt.Color(255, 255, 255));
        ButtonBlue1.setText("DR AIR");
        ButtonBlue1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonBlue1.setMaximumSize(null);
        ButtonBlue1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonBlue1ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonBlue1);

        ButtonBlue2.setBackground(new java.awt.Color(0, 131, 255));
        ButtonBlue2.setForeground(new java.awt.Color(255, 255, 255));
        ButtonBlue2.setText("VIS-B AIR");
        ButtonBlue2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonBlue2.setMaximumSize(null);
        ButtonBlue2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonBlue2ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonBlue2);

        ButtonBlue3.setBackground(new java.awt.Color(0, 131, 255));
        ButtonBlue3.setForeground(new java.awt.Color(255, 255, 255));
        ButtonBlue3.setText("POINT GEN");
        ButtonBlue3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonBlue3.setMaximumSize(null);
        ButtonBlue3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonBlue3ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonBlue3);

        ButtonBlue4.setBackground(new java.awt.Color(0, 131, 255));
        ButtonBlue4.setForeground(new java.awt.Color(255, 255, 255));
        ButtonBlue4.setText("TRACK SUBSRF");
        ButtonBlue4.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonBlue4.setMaximumSize(null);
        ButtonBlue4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonBlue4ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonBlue4);

        ButtonBlue5.setBackground(new java.awt.Color(0, 131, 255));
        ButtonBlue5.setForeground(new java.awt.Color(255, 255, 255));
        ButtonBlue5.setText("IDENT CHANGE");
        ButtonBlue5.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonBlue5.setMaximumSize(null);
        ButtonBlue5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonBlue5ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonBlue5);

        ButtonBlue6.setBackground(new java.awt.Color(0, 131, 255));
        ButtonBlue6.setForeground(new java.awt.Color(255, 255, 255));
        ButtonBlue6.setText("RAM AIR");
        ButtonBlue6.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonBlue6.setMaximumSize(null);
        ButtonBlue6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonBlue6ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonBlue6);

        ButtonBlue7.setBackground(new java.awt.Color(0, 131, 255));
        ButtonBlue7.setForeground(new java.awt.Color(255, 255, 255));
        ButtonBlue7.setText("ESM-B AIR");
        ButtonBlue7.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonBlue7.setMaximumSize(null);
        ButtonBlue7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonBlue7ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonBlue7);

        ButtonBlue8.setBackground(new java.awt.Color(0, 131, 255));
        ButtonBlue8.setForeground(new java.awt.Color(255, 255, 255));
        ButtonBlue8.setText("POINT AIR");
        ButtonBlue8.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonBlue8.setMaximumSize(null);
        ButtonBlue8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonBlue8ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonBlue8);

        ButtonBlue9.setBackground(new java.awt.Color(0, 131, 255));
        ButtonBlue9.setForeground(new java.awt.Color(255, 255, 255));
        ButtonBlue9.setText("TRACK SUBSRF");
        ButtonBlue9.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonBlue9.setMaximumSize(null);
        ButtonBlue9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonBlue9ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonBlue9);

        ButtonBlue10.setBackground(new java.awt.Color(0, 131, 255));
        ButtonBlue10.setForeground(new java.awt.Color(255, 255, 255));
        ButtonBlue10.setText("IDENT CHANGE");
        ButtonBlue10.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonBlue10.setMaximumSize(null);
        ButtonBlue10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonBlue10ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonBlue10);

        ButtonBlue11.setBackground(new java.awt.Color(0, 131, 255));
        ButtonBlue11.setForeground(new java.awt.Color(255, 255, 255));
        ButtonBlue11.setText("GEN");
        ButtonBlue11.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonBlue11.setMaximumSize(null);
        ButtonBlue11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonBlue11ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonBlue11);

        ButtonBlue12.setBackground(new java.awt.Color(0, 131, 255));
        ButtonBlue12.setForeground(new java.awt.Color(255, 255, 255));
        ButtonBlue12.setText("TRACK AIR");
        ButtonBlue12.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonBlue12.setMaximumSize(null);
        ButtonBlue12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonBlue12ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonBlue12);

        ButtonBlue13.setBackground(new java.awt.Color(0, 131, 255));
        ButtonBlue13.setForeground(new java.awt.Color(255, 255, 255));
        ButtonBlue13.setText("KEEP LOST");
        ButtonBlue13.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonBlue13.setMaximumSize(null);
        ButtonBlue13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonBlue13ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonBlue13);

        ButtonBlue14.setBackground(new java.awt.Color(0, 131, 255));
        ButtonBlue14.setForeground(new java.awt.Color(255, 255, 255));
        ButtonBlue14.setText("CHANGE SOURCE");
        ButtonBlue14.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonBlue14.setMaximumSize(null);
        ButtonBlue14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonBlue14ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonBlue14);

        ButtonBlue15.setBackground(new java.awt.Color(0, 131, 255));
        ButtonBlue15.setForeground(new java.awt.Color(255, 255, 255));
        ButtonBlue15.setText("VIPE SOURCE");
        ButtonBlue15.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonBlue15.setMaximumSize(null);
        ButtonBlue15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonBlue15ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonBlue15);

        ButtonBlue16.setBackground(new java.awt.Color(0, 131, 255));
        ButtonBlue16.setForeground(new java.awt.Color(255, 255, 255));
        ButtonBlue16.setText("GEN");
        ButtonBlue16.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonBlue16.setMaximumSize(null);
        ButtonBlue16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonBlue16ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonBlue16);

        ButtonBlue17.setBackground(new java.awt.Color(0, 131, 255));
        ButtonBlue17.setForeground(new java.awt.Color(255, 255, 255));
        ButtonBlue17.setText("REPOST IT");
        ButtonBlue17.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonBlue17.setMaximumSize(null);
        ButtonBlue17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonBlue17ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonBlue17);

        ButtonBlue18.setBackground(new java.awt.Color(0, 131, 255));
        ButtonBlue18.setForeground(new java.awt.Color(255, 255, 255));
        ButtonBlue18.setText("CORR");
        ButtonBlue18.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonBlue18.setMaximumSize(null);
        ButtonBlue18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonBlue18ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonBlue18);

        ButtonBlue19.setBackground(new java.awt.Color(0, 131, 255));
        ButtonBlue19.setForeground(new java.awt.Color(255, 255, 255));
        ButtonBlue19.setText("VIPE");
        ButtonBlue19.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonBlue19.setMaximumSize(null);
        ButtonBlue19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonBlue19ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonBlue19);

        ButtonBlue20.setBackground(new java.awt.Color(0, 131, 255));
        ButtonBlue20.setForeground(new java.awt.Color(255, 255, 255));
        ButtonBlue20.setText("IDENT CHANGE");
        ButtonBlue20.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonBlue20.setMaximumSize(null);
        ButtonBlue20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonBlue20ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonBlue20);

        ButtonRed1.setBackground(new java.awt.Color(205, 54, 74));
        ButtonRed1.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        ButtonRed1.setForeground(new java.awt.Color(255, 255, 255));
        ButtonRed1.setText("TRACK MONITOR");
        ButtonRed1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonRed1.setPreferredSize(new java.awt.Dimension(33, 19));
        ButtonRed1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonRed1ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonRed1);

        ButtonRed2.setBackground(new java.awt.Color(205, 54, 74));
        ButtonRed2.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        ButtonRed2.setForeground(new java.awt.Color(255, 255, 255));
        ButtonRed2.setText("CLOSE CONTROL");
        ButtonRed2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonRed2.setMinimumSize(new java.awt.Dimension(89, 19));
        ButtonRed2.setPreferredSize(new java.awt.Dimension(89, 19));
        ButtonRed2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonRed2ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonRed2);

        ButtonRed3.setBackground(new java.awt.Color(205, 54, 74));
        ButtonRed3.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        ButtonRed3.setForeground(new java.awt.Color(255, 255, 255));
        ButtonRed3.setText("SEARCH TRACK");
        ButtonRed3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonRed3.setMinimumSize(new java.awt.Dimension(89, 19));
        ButtonRed3.setPreferredSize(new java.awt.Dimension(33, 19));
        ButtonRed3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonRed3ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonRed3);

        ButtonRed4.setBackground(new java.awt.Color(205, 54, 74));
        ButtonRed4.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        ButtonRed4.setForeground(new java.awt.Color(255, 255, 255));
        ButtonRed4.setText("TRACK FILTER");
        ButtonRed4.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonRed4.setMinimumSize(new java.awt.Dimension(89, 19));
        ButtonRed4.setPreferredSize(new java.awt.Dimension(33, 19));
        ButtonRed4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonRed4ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonRed4);

        ButtonRed5.setBackground(new java.awt.Color(205, 54, 74));
        ButtonRed5.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        ButtonRed5.setForeground(new java.awt.Color(255, 255, 255));
        ButtonRed5.setText("DATA REQUEST");
        ButtonRed5.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ButtonRed5.setMinimumSize(new java.awt.Dimension(89, 19));
        ButtonRed5.setPreferredSize(new java.awt.Dimension(33, 19));
        ButtonRed5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonRed5ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonRed5);

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void connectButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_connectButtonActionPerformed
        connect();
    }//GEN-LAST:event_connectButtonActionPerformed

    private void num1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_num1ActionPerformed
        JTextField comp = (JTextField) KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        if (comp instanceof JTextField) {
            JTextField jt = (JTextField) comp;
            jt.setText(jt.getText() + "1");
        }
    }//GEN-LAST:event_num1ActionPerformed

    private void num2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_num2ActionPerformed
        JTextField comp = (JTextField) KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        if (comp instanceof JTextField) {
            JTextField jt = (JTextField) comp;
            jt.setText(jt.getText() + "2");
        }
    }//GEN-LAST:event_num2ActionPerformed

    private void num3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_num3ActionPerformed
        JTextField comp = (JTextField) KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        if (comp instanceof JTextField) {
            JTextField jt = (JTextField) comp;
            jt.setText(jt.getText() + "3");
        }
    }//GEN-LAST:event_num3ActionPerformed

    private void num4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_num4ActionPerformed
        JTextField comp = (JTextField) KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        if (comp instanceof JTextField) {
            JTextField jt = (JTextField) comp;
            jt.setText(jt.getText() + "4");
        }
    }//GEN-LAST:event_num4ActionPerformed

    private void num5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_num5ActionPerformed
        JTextField comp = (JTextField) KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        if (comp instanceof JTextField) {
            JTextField jt = (JTextField) comp;
            jt.setText(jt.getText() + "5");
        }
    }//GEN-LAST:event_num5ActionPerformed

    private void num6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_num6ActionPerformed
        JTextField comp = (JTextField) KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        if (comp instanceof JTextField) {
            JTextField jt = (JTextField) comp;
            jt.setText(jt.getText() + "6");
        }
    }//GEN-LAST:event_num6ActionPerformed

    private void num7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_num7ActionPerformed
        JTextField comp = (JTextField) KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        if (comp instanceof JTextField) {
            JTextField jt = (JTextField) comp;
            jt.setText(jt.getText() + "7");
        }
    }//GEN-LAST:event_num7ActionPerformed

    private void num8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_num8ActionPerformed
        JTextField comp = (JTextField) KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        if (comp instanceof JTextField) {
            JTextField jt = (JTextField) comp;
            jt.setText(jt.getText() + "8");
        }
    }//GEN-LAST:event_num8ActionPerformed

    private void num9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_num9ActionPerformed
        JTextField comp = (JTextField) KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        if (comp instanceof JTextField) {
            JTextField jt = (JTextField) comp;
            jt.setText(jt.getText() + "9");
        }
    }//GEN-LAST:event_num9ActionPerformed

    private void num0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_num0ActionPerformed
        JTextField comp = (JTextField) KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        if (comp instanceof JTextField) {
            JTextField jt = (JTextField) comp;
            jt.setText(jt.getText() + "0");
        }
    }//GEN-LAST:event_num0ActionPerformed

    private void numCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_numCActionPerformed
        JTextField comp = (JTextField) KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        if (comp instanceof JTextField) {
            JTextField jt = (JTextField) comp;
            String str = jt.getText().trim();
            if (jt.getText() != null) {
                str = str.substring(0, str.length() - 1);
            }
            jt.setText(str);
        }
    }//GEN-LAST:event_numCActionPerformed

    private void numDotActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_numDotActionPerformed
        JTextField comp = (JTextField) KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        if (comp instanceof JTextField) {
            JTextField jt = (JTextField) comp;
            jt.setText(jt.getText() + ".");
        }
    }//GEN-LAST:event_numDotActionPerformed

    private void ButtonRed5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonRed5ActionPerformed
        sendMessage(ButtonRed5.getText());
    }//GEN-LAST:event_ButtonRed5ActionPerformed

    private void ButtonRed4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonRed4ActionPerformed
        sendMessage(ButtonRed4.getText());
    }//GEN-LAST:event_ButtonRed4ActionPerformed

    private void ButtonRed3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonRed3ActionPerformed
        sendMessage(ButtonRed3.getText());
    }//GEN-LAST:event_ButtonRed3ActionPerformed

    private void ButtonRed2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonRed2ActionPerformed
        sendMessage(ButtonRed2.getText());
    }//GEN-LAST:event_ButtonRed2ActionPerformed

    private void ButtonRed1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonRed1ActionPerformed
        sendMessage(ButtonRed1.getText());
    }//GEN-LAST:event_ButtonRed1ActionPerformed

    private void ButtonBlue20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonBlue20ActionPerformed
        sendMessage(getTempBtnBlue() + ":" + ButtonBlue20.getText()); // TODO add your handling code here:
    }//GEN-LAST:event_ButtonBlue20ActionPerformed

    private void ButtonBlue19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonBlue19ActionPerformed
        sendMessage(getTempBtnBlue() + ":" + ButtonBlue19.getText());
    }//GEN-LAST:event_ButtonBlue19ActionPerformed

    private void ButtonBlue18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonBlue18ActionPerformed
        sendMessage(getTempBtnBlue() + ":" + ButtonBlue18.getText());
    }//GEN-LAST:event_ButtonBlue18ActionPerformed

    private void ButtonBlue17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonBlue17ActionPerformed
        sendMessage(getTempBtnBlue() + ":" + ButtonBlue17.getText());
    }//GEN-LAST:event_ButtonBlue17ActionPerformed

    private void ButtonBlue16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonBlue16ActionPerformed
        sendMessage(getTempBtnBlue() + ":" + ButtonBlue16.getText());// TODO add your handling code here:
    }//GEN-LAST:event_ButtonBlue16ActionPerformed

    private void ButtonBlue15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonBlue15ActionPerformed
        sendMessage(getTempBtnBlue() + ":" + ButtonBlue15.getText());
    }//GEN-LAST:event_ButtonBlue15ActionPerformed

    private void ButtonBlue14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonBlue14ActionPerformed
        sendMessage(getTempBtnBlue() + ":" + ButtonBlue14.getText());
    }//GEN-LAST:event_ButtonBlue14ActionPerformed

    private void ButtonBlue13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonBlue13ActionPerformed
        sendMessage(getTempBtnBlue() + ":" + ButtonBlue13.getText());
    }//GEN-LAST:event_ButtonBlue13ActionPerformed

    private void ButtonBlue12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonBlue12ActionPerformed
        sendMessage(getTempBtnBlue() + ":" + ButtonBlue12.getText());
    }//GEN-LAST:event_ButtonBlue12ActionPerformed

    private void ButtonBlue11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonBlue11ActionPerformed
        sendMessage(getTempBtnBlue() + ":" + ButtonBlue11.getText());
    }//GEN-LAST:event_ButtonBlue11ActionPerformed

    private void ButtonBlue10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonBlue10ActionPerformed
        sendMessage(getTempBtnBlue() + ":" + ButtonBlue10.getText());
    }//GEN-LAST:event_ButtonBlue10ActionPerformed

    private void ButtonBlue9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonBlue9ActionPerformed
        sendMessage(getTempBtnBlue() + ":" + ButtonBlue9.getText());
    }//GEN-LAST:event_ButtonBlue9ActionPerformed

    private void ButtonBlue8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonBlue8ActionPerformed
        sendMessage(getTempBtnBlue() + ":" + ButtonBlue8.getText());
    }//GEN-LAST:event_ButtonBlue8ActionPerformed

    private void ButtonBlue7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonBlue7ActionPerformed
        sendMessage(getTempBtnBlue() + ":" + ButtonBlue7.getText());
    }//GEN-LAST:event_ButtonBlue7ActionPerformed

    private void ButtonBlue6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonBlue6ActionPerformed
        sendMessage(getTempBtnBlue() + ":" + ButtonBlue6.getText());
    }//GEN-LAST:event_ButtonBlue6ActionPerformed

    private void ButtonBlue5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonBlue5ActionPerformed
        sendMessage(getTempBtnBlue() + ":" + ButtonBlue5.getText());
    }//GEN-LAST:event_ButtonBlue5ActionPerformed

    private void ButtonBlue4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonBlue4ActionPerformed
        sendMessage(getTempBtnBlue() + ":" + ButtonBlue4.getText());
    }//GEN-LAST:event_ButtonBlue4ActionPerformed

    private void ButtonBlue3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonBlue3ActionPerformed
        sendMessage(getTempBtnBlue() + ":" + ButtonBlue3.getText()); // TODO add your handling code here:
    }//GEN-LAST:event_ButtonBlue3ActionPerformed

    private void ButtonBlue2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonBlue2ActionPerformed
        sendMessage(getTempBtnBlue() + ":" + ButtonBlue2.getText());
    }//GEN-LAST:event_ButtonBlue2ActionPerformed

    private void ButtonBlue1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonBlue1ActionPerformed
        sendMessage(getTempBtnBlue() + ":" + ButtonBlue1.getText());
    }//GEN-LAST:event_ButtonBlue1ActionPerformed

    private void ButtonGreen15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonGreen15ActionPerformed
        clearTempBtnBlue();
        clearButtonBlue();
        System.out.println("No Action");
    }//GEN-LAST:event_ButtonGreen15ActionPerformed

    private void ButtonGreen14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonGreen14ActionPerformed
        clearTempBtnBlue();
        if (ButtonGreen14.getText() != null) {
            setBtnGunEngage();
            setTempBtnBlue(ButtonGreen12.getText());
        } else {
            clearButtonBlue();
            System.out.println("No Action");
        }
    }//GEN-LAST:event_ButtonGreen14ActionPerformed

    private void ButtonGreen13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonGreen13ActionPerformed
        clearTempBtnBlue();
        clearButtonBlue();
        System.out.println("No Action");
    }//GEN-LAST:event_ButtonGreen13ActionPerformed

    private void ButtonGreen12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonGreen12ActionPerformed
        clearTempBtnBlue();
        clearButtonBlue();
        System.out.println("No Action");
    }//GEN-LAST:event_ButtonGreen12ActionPerformed

    private void ButtonGreen11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonGreen11ActionPerformed
        clearTempBtnBlue();
        if (ButtonGreen11.getText() != null) {
            setBtnTpo();
            setTempBtnBlue(ButtonGreen11.getText());
        } else {
            clearButtonBlue();
            System.out.println("No Action");
        }
    }//GEN-LAST:event_ButtonGreen11ActionPerformed

    private void ButtonGreen10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonGreen10ActionPerformed
        clearTempBtnBlue();
        if (ButtonGreen10.getText() != null) {
            setBtndataLink();
            setTempBtnBlue(ButtonGreen10.getText());
        } else {
            clearButtonBlue();
            System.out.println("No Action");
        }
    }//GEN-LAST:event_ButtonGreen10ActionPerformed

    private void ButtonGreen9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonGreen9ActionPerformed
        clearTempBtnBlue();
        clearButtonBlue();
        System.out.println("No Action");
    }//GEN-LAST:event_ButtonGreen9ActionPerformed

    private void ButtonGreen8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonGreen8ActionPerformed
        clearTempBtnBlue();
        clearButtonBlue();
        System.out.println("No Action");
    }//GEN-LAST:event_ButtonGreen8ActionPerformed

    private void ButtonGreen7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonGreen7ActionPerformed
        clearTempBtnBlue();
        if (ButtonGreen7.getText() != null) {
            setBtnLiod();
            setTempBtnBlue(ButtonGreen7.getText());
        } else {
            clearButtonBlue();
            System.out.println("No Action");
        }
    }//GEN-LAST:event_ButtonGreen7ActionPerformed

    private void ButtonGreen6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonGreen6ActionPerformed
        clearTempBtnBlue();
        if (ButtonGreen6.getText() != null) {
            setBtnHms();
            setTempBtnBlue(ButtonGreen6.getText());
        } else {
            clearButtonBlue();
            System.out.println("No Action");
        }
    }//GEN-LAST:event_ButtonGreen6ActionPerformed

    private void ButtonGreen5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonGreen5ActionPerformed
        clearTempBtnBlue();
        if (ButtonGreen5.getText() != null) {
            setBtnIdentChange();
            setTempBtnBlue(ButtonGreen5.getText());
        } else {
            clearButtonBlue();
            System.out.println("No Action");
        }
    }//GEN-LAST:event_ButtonGreen5ActionPerformed

    private void ButtonGreen4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonGreen4ActionPerformed
        clearTempBtnBlue();
        if (ButtonGreen4.getText() != null) {
            setBtnTrackSubSurf();
            setTempBtnBlue(ButtonGreen4.getText());
        } else {
            clearButtonBlue();
            System.out.println("No Action");
        }
    }//GEN-LAST:event_ButtonGreen4ActionPerformed

    private void ButtonGreen3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonGreen3ActionPerformed
        clearTempBtnBlue();
        if (ButtonGreen3.getText() != null) {
            setBtnTrackSurf();
            setTempBtnBlue(ButtonGreen3.getText());
        } else {
            clearButtonBlue();
            System.out.println("No Action");
        }
    }//GEN-LAST:event_ButtonGreen3ActionPerformed

    private void ButtonGreen2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonGreen2ActionPerformed
        clearTempBtnBlue();
        if (ButtonGreen2.getText() != null) {
            setBtnTrackAir();
            setTempBtnBlue(ButtonGreen2.getText());
        } else {
            clearButtonBlue();
            System.out.println("No Action");
        }
    }//GEN-LAST:event_ButtonGreen2ActionPerformed

    private void ButtonGreen1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonGreen1ActionPerformed
        clearTempBtnBlue();
        if (ButtonGreen1.getText() != null) {
            setBtnGen();
            setTempBtnBlue(ButtonGreen1.getText());
        } else {
            clearButtonBlue();
            System.out.println("No Action");
        }
    }//GEN-LAST:event_ButtonGreen1ActionPerformed

    private void jPanel1AncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_jPanel1AncestorAdded
        dialogConnection();
    }//GEN-LAST:event_jPanel1AncestorAdded

    private void closeApplicationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeApplicationActionPerformed
        int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog(null, "Are you sure to Exit?", "Warning", dialogButton);
        if (dialogResult == JOptionPane.YES_OPTION) {
            System.exit(0);
        }else{
            dialogConnection();
        }
    }//GEN-LAST:event_closeApplicationActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ButtonBlue1;
    private javax.swing.JButton ButtonBlue10;
    private javax.swing.JButton ButtonBlue11;
    private javax.swing.JButton ButtonBlue12;
    private javax.swing.JButton ButtonBlue13;
    private javax.swing.JButton ButtonBlue14;
    private javax.swing.JButton ButtonBlue15;
    private javax.swing.JButton ButtonBlue16;
    private javax.swing.JButton ButtonBlue17;
    private javax.swing.JButton ButtonBlue18;
    private javax.swing.JButton ButtonBlue19;
    private javax.swing.JButton ButtonBlue2;
    private javax.swing.JButton ButtonBlue20;
    private javax.swing.JButton ButtonBlue3;
    private javax.swing.JButton ButtonBlue4;
    private javax.swing.JButton ButtonBlue5;
    private javax.swing.JButton ButtonBlue6;
    private javax.swing.JButton ButtonBlue7;
    private javax.swing.JButton ButtonBlue8;
    private javax.swing.JButton ButtonBlue9;
    private javax.swing.JButton ButtonGreen1;
    private javax.swing.JButton ButtonGreen10;
    private javax.swing.JButton ButtonGreen11;
    private javax.swing.JButton ButtonGreen12;
    private javax.swing.JButton ButtonGreen13;
    private javax.swing.JButton ButtonGreen14;
    private javax.swing.JButton ButtonGreen15;
    private javax.swing.JButton ButtonGreen2;
    private javax.swing.JButton ButtonGreen3;
    private javax.swing.JButton ButtonGreen4;
    private javax.swing.JButton ButtonGreen5;
    private javax.swing.JButton ButtonGreen6;
    private javax.swing.JButton ButtonGreen7;
    private javax.swing.JButton ButtonGreen8;
    private javax.swing.JButton ButtonGreen9;
    private javax.swing.JButton ButtonRed1;
    private javax.swing.JButton ButtonRed2;
    private javax.swing.JButton ButtonRed3;
    private javax.swing.JButton ButtonRed4;
    private javax.swing.JButton ButtonRed5;
    private javax.swing.JButton closeApplication;
    private javax.swing.JButton connectButton;
    private javax.swing.JTextField ipField;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JButton num0;
    private javax.swing.JButton num1;
    private javax.swing.JButton num2;
    private javax.swing.JButton num3;
    private javax.swing.JButton num4;
    private javax.swing.JButton num5;
    private javax.swing.JButton num6;
    private javax.swing.JButton num7;
    private javax.swing.JButton num8;
    private javax.swing.JButton num9;
    private javax.swing.JButton numC;
    private javax.swing.JButton numDot;
    private javax.swing.JTextField portField;
    // End of variables declaration//GEN-END:variables

}
